import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:musically/controller/home_controller.dart';
import 'package:musically/res/style.dart';

import '../controller/search_page_controller.dart';
import '../main.dart';


///Common widgets-----------------------------------------------------

class CustomText extends StatelessWidget {
  String text;
  double? fontSize;
  Color? color;
  double? lineSpace;

  CustomText(
      {super.key,
      required this.text,
      this.fontSize,
      this.lineSpace,
      this.color});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: 1,
      style: TextStyle(
          color: color ?? appTextWhite,
          letterSpacing: lineSpace ?? 0,
          fontSize: fontSize ?? width * 0.04),
    );
  }
}

Widget bottomBar() => BottomNavigationBar(
        backgroundColor: Colors.grey.shade800,
        type: BottomNavigationBarType.fixed,
        currentIndex: 0,
        selectedItemColor: Colors.black,
        iconSize: 30,
        onTap: (index) {},
        items: const [
          BottomNavigationBarItem(
            label: '',
            icon: Icon(Icons.speed, color: appTextWhite),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Icon(Icons.pause, color: appTextWhite),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Icon(Icons.bookmark, color: appTextWhite),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Icon(Icons.fast_forward, color: appTextWhite),
          ),
        ]);

Widget functionButton({String? text, double? horizontal, double? vertical, double? fontSize}) => Container(
      padding: EdgeInsets.symmetric(
          horizontal: horizontal?? width * 0.04, vertical: vertical?? height * 0.004),
      decoration: BoxDecoration(
          color: Colors.purple,
          borderRadius: BorderRadius.all(Radius.circular(width * 0.04))),
      child: Center(
        child: CustomText(text: text ?? '', fontSize: fontSize??width * 0.04),
      ),
    );

Widget profileCircle({double? size}) => Container(
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: appTextWhite, width: 1)),
      child:
          Icon(Icons.person, color: appTextWhite, size: size ?? width * 0.05),
    );

///-------------------------------------------------------------------

///Home page widgets--------------------------------------------------

Widget appbar() => Container(
      padding: EdgeInsets.only(top: height * 0.03, bottom: height * 0.05),
      child: Row(
        children: [
          CustomText(
            text: 'Good Afternoon, Sachin!',
          ),
          const Spacer(),
          Icon(
            Icons.headset,
            color: appTextWhite,
            size: width * 0.05,
          ),
          SizedBox(width: width * 0.04),
          profileCircle(size: width * 0.05)
        ],
      ),
    );

Widget musicGenreBox() => GetX<HomeController>(builder: (c) {
      return SizedBox(
        height: height * 0.15,
        width: width,
        child: c.isLoading.value
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: c.musicGenreList.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return Stack(
                    children: [
                      Container(
                        height: height * 0.15,
                        width: width * 0.3,
                        margin: EdgeInsets.only(right: width * 0.03),
                        decoration: BoxDecoration(
                            color: appTextWhite,
                            borderRadius: BorderRadius.all(
                                Radius.circular(width * 0.04))),
                        child: Center(
                          child: Image.asset('assets/images/jazz_icon.png'),
                        ),
                      ),
                      Container(
                        height: height * 0.15,
                        width: width * 0.3,
                        decoration: BoxDecoration(
                            color: appLightPurple,
                            borderRadius: BorderRadius.all(
                                Radius.circular(width * 0.04))),
                        child: Center(
                          child: CustomText(
                              text: c.musicGenreList[index].name ?? '',
                              fontSize: width * 0.04),
                        ),
                      ),
                    ],
                  );
                }),
      );
    });

Widget popularSongListView() => GetBuilder<HomeController>(builder: (c) {
      return c.isLoading.value
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.only(top: height * 0.04),
              child: SizedBox(
                height: height,
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: 13,
                  itemBuilder: (BuildContext context, int index) => Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.01),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        profileCircle(size: width * 0.08),
                        SizedBox(width: width * 0.04),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                                width: width * 0.55,
                                child:
                                    CustomText(text: c.recordingsList[index])),
                            CustomText(
                                text: c.artistNameList[index],
                                color: Colors.grey),
                          ],
                        ),
                        const Spacer(),
                        Icon(
                          Icons.play_arrow,
                          color: appTextWhite,
                          size: width * 0.08,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
    });

///-------------------------------------------------------------------

///Search page widgets -----------------------------------------------

Widget searchBar(SearchPageController controller) => Container(
      padding: EdgeInsets.only(top: height * 0.03, bottom: height * 0.05),
      child: Row(
        children: [
          SizedBox(
            width: width * 0.7,
            height: height * 0.043,
            child: TextField(
              controller: controller.searchController,
              style: TextStyle(
                  color: appTextWhite, fontSize: height * 0.023, height: 0.8),
              onChanged: (value) {
                controller.getArtistBySearchQuery(searchText: value);
              },
              decoration: InputDecoration(
                  fillColor: Colors.grey.shade900,
                  filled: true,
                  focusedBorder: textFieldDecoration,
                  prefixIcon: Icon(
                    Icons.search,
                    color: appTextWhite,
                    size: width * 0.06,
                  ),
                  border: textFieldDecoration),
            ),
          ),
          const Spacer(),
          Icon(
            Icons.notifications,
            color: appTextWhite,
            size: width * 0.05,
          ),
          SizedBox(width: width * 0.04),
          profileCircle(size: width * 0.05)
        ],
      ),
    );

Widget searchResultDialog(SearchPageController controller, BuildContext context) => Container(
    decoration: const BoxDecoration(
        color: Colors.white24,
        borderRadius: BorderRadius.all(Radius.circular(10))),
    child: Column(
      children: [
        InkWell(
          onTap: () {
            controller.searchResultList.value = [];
            FocusScope.of(context).unfocus();
            controller.searchController.clear();
            controller.update();
          },
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: CustomText(text: 'Close  X', color: Colors.redAccent,),
          ),
        ),
        Container(
          height: height * 0.3,
          width: width * 0.9,
          color: Colors.grey,
          child: ListView(
                  children: controller.searchResultList
                      .map((e) => Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: width * 0.02,
                                vertical: height * 0.008),
                            child: InkWell(
                              onTap: () {
                                controller.getSongByArtist(
                                    artistName: e.name ?? '');
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  profileCircle(size: width * 0.05),
                                  SizedBox(width: width * 0.05),
                                  CustomText(
                                      text: e.name.toString(),
                                      lineSpace: 1,
                                      fontSize: width * 0.045),
                                ],
                              ),
                            ),
                          ))
                      .toList(),
                ),
        ),
      ],
    ));

Widget recentSearchListView(SearchPageController controller) => Padding(
  padding: EdgeInsets.only(bottom: height * 0.02),
  child: Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CustomText(
              text: 'Recent Searches', fontSize: width * 0.055),
          InkWell(
              onTap: () {
                prefs.setStringList('recentSearchList', []);
                controller.recentSearchList.value.clear();
                controller.update();
              },
              child: functionButton(text: 'Clear'))
        ],
      ),
      SizedBox(height: height * 0.015),
      GetBuilder<SearchPageController>(
        builder: (controller) {
          return Column(
              children: controller.recentSearchList.value.isNotEmpty?
              controller.recentSearchList.value
                  .map(
                    (e) => InkWell(
                  onTap: () {
                    controller.getSongByArtist(artistName: e);
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: height * 0.01),
                          child: profileCircle(size: width * 0.06),
                        ),
                        SizedBox(width: width * 0.04),
                        CustomText(text: e.toString())
                      ]),
                ),
              ).toList():
              [CustomText(text: 'Nothing is available',color: Colors.grey,)]);
        }
      ),
    ],
  ),
);

Widget popularGenreBox() => GetX<HomeController>(builder: (c) {
  return Column(
    children: [
      Padding(
        padding: EdgeInsets.only(bottom: height * 0.02, top: height * 0.05),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
                text: 'Popular Genres', fontSize: width * 0.055),
          ],
        ),
      ),
      SizedBox(
        height: height,
        width: width,
        child: c.isLoading.value
            ? const Center(
          child: CircularProgressIndicator(),
        ) : GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
                mainAxisSpacing: height*0.03,
                crossAxisSpacing: height*0.02,
                childAspectRatio: 1.2),
            physics: const NeverScrollableScrollPhysics(),
            itemCount: c.musicGenreList.length,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white10,
                        borderRadius: BorderRadius.all(
                            Radius.circular(width * 0.04))),
                    child: Center(
                      child: Image.asset('assets/images/jazz_icon.png'),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white24,
                        borderRadius: BorderRadius.all(
                            Radius.circular(width * 0.04))),
                    child: Center(
                      child: CustomText(
                          text: c.musicGenreList[index].name ?? '',
                          fontSize: width * 0.06),
                    ),
                  ),
                ],
              );
            }),
      ),
    ],
  );
});

OutlineInputBorder get textFieldDecoration => OutlineInputBorder(
    borderSide: const BorderSide(color: appBlack),
    borderRadius: BorderRadius.all(Radius.circular(height * 0.03)));

///--------------------------------------------------------------------------


///Search result page widgets -----------------------------------------------

Widget searchResultTitle(String artistName) => Column(
  mainAxisAlignment: MainAxisAlignment.start,
  crossAxisAlignment: CrossAxisAlignment.start,
  children: [
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        CustomText(text: "Search result for"),
        const Icon(Icons.play_arrow, color: appTextWhite)
      ],
    ),
    CustomText(text: '"$artistName"', fontSize: 20,),
  ],
);

Widget songListView(SearchPageController controller, int index) => Container(
  margin: EdgeInsets.symmetric(vertical: height * 0.01),
  padding: EdgeInsets.symmetric(vertical: height * 0.015,
      horizontal: height * 0.015),
  decoration: BoxDecoration(
      color: Colors.grey.shade900,
      borderRadius: BorderRadius.all(Radius.circular(height * 0.03))
  ),
  height: height * 0.21,
  width: width,
  child: Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Container(
        decoration: BoxDecoration(
            color: appBlack,
            borderRadius: BorderRadius.all(Radius.circular(height * 0.03))
        ),
        height: height * 0.14,
        width: width,
        child: Image.asset('assets/images/music_band.png', fit: BoxFit.fill,),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
              width: width * 0.5,
              child: CustomText(text: controller.songList[index].title??'')),
          functionButton(text: 'Play', vertical: height*0.0008,
              horizontal: width*0.05, fontSize: 11)
        ],
      )
    ],
  ),
);

///--------------------------------------------------------------------------
