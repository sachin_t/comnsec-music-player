
import 'dart:convert';
import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:musically/model/get_popular_songs_model.dart';
import 'package:musically/model/get_song_by_artist_model.dart';
import 'package:musically/service/urls.dart';
import '../model/get_Artist_by_search_model.dart';
import '../model/get_all_genre_model.dart';

class ApiService {

  static Future<dynamic> getAllGenre() async {
    try {
      Map<String, String> headers = {
        "Accept": "application/json",
      };
      http.Response response = await http.get(
          Uri.parse(urlGetAllGenre), headers: headers);
      if (kDebugMode) {
        print('getAllGenre= ${response.body}');
      }
      if (response.statusCode == 200) {
        return getAllGenreModelFromJson(response.body);
      }
    } catch (e) {
      Get.snackbar('Error!', "Something went wrong");
    }
  }

  static Future<GetPopularSongsModel> getPopularSongs() async {

      Map<String, String> headers = {
        "Accept": "application/json",
      };
      http.Response response = await http.get(
          Uri.parse('${urlGetPoplarSongs}michael%20jackson'), headers: headers);
      if (kDebugMode) {
        print('getPopularSongs= ${response.body}');
      }
      if (response.statusCode == 200) {
        return getPopularSongsModelFromJson(response.body);
      } else{
      Get.snackbar('Error!', "Something went wrong");
      return getPopularSongsModelFromJson(response.body);
    }
  }

  static Future<GetArtistBySearchModel> getArtistBySearchQuery({required String searchText}) async {

      Map<String, String> headers = {
        "Accept": "application/json",
      };
      http.Response response = await http.get(
          Uri.parse(urlSearchByArtists+searchText), headers: headers);
      if (kDebugMode) {
        print('getArtistBySearchQuery= ${response.body}');
      }
      if (response.statusCode == 200) {
        return getArtistBySearchModelFromJson(response.body);
      } else{
      Get.snackbar('Error!', "Something went wrong");
      return getArtistBySearchModelFromJson(response.body);
    }
  }

  static Future<GetSongByArtistModel> getSongByArtist({required String artistName}) async {

      Map<String, String> headers = {
        "Accept": "application/json",
      };
      http.Response response = await http.get(
          Uri.parse(urlGetPoplarSongs+artistName), headers: headers);
      if (kDebugMode) {
        print('getSongByArtist= ${response.body}');
      }
      if (response.statusCode == 200) {
        return GetSongByArtistModel.fromJson(jsonDecode(response.body));
      } else{
      Get.snackbar('Error!', "Something went wrong");
      return GetSongByArtistModel.fromJson(jsonDecode(response.body));
    }
  }

}
