import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/search_page_controller.dart';
import '../res/style.dart';
import '../widgets/widgets.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appBlack,
      body: GetX<SearchPageController>(
          init: SearchPageController(),
          builder: (c) {
            return Container(
                padding:
                    EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                child: ListView(children: [
                  searchBar(c),
                  Visibility(
                      visible: c.searchResultList.isNotEmpty,
                      child: searchResultDialog(c, context)),
                  recentSearchListView(c),
                  popularGenreBox(),
                ]));
          }),
      bottomNavigationBar: bottomBar(),
    );
  }
}
