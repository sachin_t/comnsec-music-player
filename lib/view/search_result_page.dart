import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:musically/controller/search_page_controller.dart';
import 'package:musically/widgets/widgets.dart';

import '../res/style.dart';

class SearchResultPage extends StatelessWidget {
    SearchResultPage({Key? key, required this.artistName}) : super(key: key);
    String artistName;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appBlack,
      body: Container(
        margin: EdgeInsets.symmetric(vertical: height * 0.02),
          padding: EdgeInsets.only(
          left: width * 0.05,
          right: width * 0.05),
        child: GetBuilder<SearchPageController>(
          init: SearchPageController(),
          builder: (c) {
            return ListView(
              children: [
                searchResultTitle(artistName),
                SizedBox(
                  height: height,
                  width: width,
                  child: ListView.builder(
                    itemCount: c.songList.length,
                      itemBuilder: (context, index) => songListView(c, index)),
                ),
              ],
            );
          }
        ),
      ),
      bottomNavigationBar: bottomBar(),
    );
  }
}
