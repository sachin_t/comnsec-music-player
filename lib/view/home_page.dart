import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:musically/view/search_page.dart';
import 'package:musically/widgets/widgets.dart';

import '../res/style.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appBlack,
      body: Container(
        padding: EdgeInsets.only(
        left: width * 0.06,
        right: width * 0.06),
        child: ListView(
          children: [
            appbar(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText(
                    text: 'Discover Music',
                fontSize: width * 0.055),
                InkWell(
                  onTap: () => Get.to(const SearchPage()),
                    child: functionButton(text: 'Explore'))
              ],
            ),
            SizedBox(height: height * 0.02),
            musicGenreBox(),
            SizedBox(height: height * 0.05),
            CustomText(text: 'Popular Songs',
                fontSize: width * 0.055),
            popularSongListView(),
            SizedBox(height: height * 0.05)
          ],
        ),
      ),
      bottomNavigationBar: bottomBar(),
    );
  }
}
