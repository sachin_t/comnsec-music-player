// To parse this JSON data, do
//
//     final getSongByArtistModel = getSongByArtistModelFromJson(jsonString);

import 'dart:convert';

import 'package:get/get.dart';

GetSongByArtistModel getSongByArtistModelFromJson(String str) => GetSongByArtistModel.fromJson(json.decode(str));

String getSongByArtistModelToJson(GetSongByArtistModel data) => json.encode(data.toJson());

class GetSongByArtistModel {
  DateTime? created;
  int? count;
  int? offset;
  List<Recording>? recordings;

  GetSongByArtistModel({
    this.created,
    this.count,
    this.offset,
    this.recordings,
  });

  factory GetSongByArtistModel.fromJson(Map<String, dynamic> json) => GetSongByArtistModel(
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
    count: json["count"],
    offset: json["offset"],
    recordings: json["recordings"] == null ? [] : List<Recording>.from(json["recordings"]!.map((x) => Recording.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "created": created?.toIso8601String(),
    "count": count,
    "offset": offset,
    "recordings": recordings == null ? [] : List<dynamic>.from(recordings!.map((x) => x.toJson())),
  };
}

class Recording {
  String? id;
  int? score;
  String? title;
  int? length;
  dynamic video;
  List<RecordingArtistCredit>? artistCredit;
  String? firstReleaseDate;
  List<Release>? releases;
  List<String>? isrcs;
  List<Tag>? tags;

  Recording({
    this.id,
    this.score,
    this.title,
    this.length,
    this.video,
    this.artistCredit,
    this.firstReleaseDate,
    this.releases,
    this.isrcs,
    this.tags,
  });

  factory Recording.fromJson(Map<String, dynamic> json) => Recording(
    id: json["id"],
    score: json["score"],
    title: json["title"],
    length: json["length"],
    video: json["video"],
    artistCredit: json["artist-credit"] == null ? [] : List<RecordingArtistCredit>.from(json["artist-credit"]!.map((x) => RecordingArtistCredit.fromJson(x))),
    firstReleaseDate: json["first-release-date"],
    releases: json["releases"] == null ? [] : List<Release>.from(json["releases"]!.map((x) => Release.fromJson(x))),
    isrcs: json["isrcs"] == null ? [] : List<String>.from(json["isrcs"]!.map((x) => x)),
    tags: json["tags"] == null ? [] : List<Tag>.from(json["tags"]!.map((x) => Tag.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "score": score,
    "title": title,
    "length": length,
    "video": video,
    "artist-credit": artistCredit == null ? [] : List<dynamic>.from(artistCredit!.map((x) => x.toJson())),
    "first-release-date": firstReleaseDate,
    "releases": releases == null ? [] : List<dynamic>.from(releases!.map((x) => x.toJson())),
    "isrcs": isrcs == null ? [] : List<dynamic>.from(isrcs!.map((x) => x)),
    "tags": tags == null ? [] : List<dynamic>.from(tags!.map((x) => x.toJson())),
  };
}

class RecordingArtistCredit {
  String? name;
  Artist? artist;
  String? joinphrase;

  RecordingArtistCredit({
    this.name,
    this.artist,
    this.joinphrase,
  });

  factory RecordingArtistCredit.fromJson(Map<String, dynamic> json) => RecordingArtistCredit(
    name: json["name"],
    artist: json["artist"] == null ? null : Artist.fromJson(json["artist"]),
    joinphrase: json["joinphrase"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "artist": artist?.toJson(),
    "joinphrase": joinphrase,
  };
}

class Artist {
  String? id;
  String? name;
  String? sortName;
  String? disambiguation;
  List<Alias>? aliases;
  List<String>? iso31661Codes;

  Artist({
    this.id,
    this.name,
    this.sortName,
    this.disambiguation,
    this.aliases,
    this.iso31661Codes,
  });

  factory Artist.fromJson(Map<String, dynamic> json) => Artist(
    id: json["id"],
    name: json["name"],
    sortName: json["sort-name"],
    disambiguation: json["disambiguation"],
    aliases: json["aliases"] == null ? [] : List<Alias>.from(json["aliases"]!.map((x) => Alias.fromJson(x))),
    iso31661Codes: json["iso-3166-1-codes"] == null ? [] : List<String>.from(json["iso-3166-1-codes"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "sort-name": sortName,
    "disambiguation": disambiguation,
    "aliases": aliases == null ? [] : List<dynamic>.from(aliases!.map((x) => x.toJson())),
    "iso-3166-1-codes": iso31661Codes == null ? [] : List<dynamic>.from(iso31661Codes!.map((x) => x)),
  };
}

class Alias {
  String? sortName;
  String? name;
  String? locale;
  String? type;
  bool? primary;
  dynamic beginDate;
  dynamic endDate;
  String? typeId;

  Alias({
    this.sortName,
    this.name,
    this.locale,
    this.type,
    this.primary,
    this.beginDate,
    this.endDate,
    this.typeId,
  });

  factory Alias.fromJson(Map<String, dynamic> json) => Alias(
    sortName: json["sort-name"],
    name: json["name"],
    locale: json["locale"],
    type: json["type"],
    primary: json["primary"],
    beginDate: json["begin-date"],
    endDate: json["end-date"],
    typeId: json["type-id"],
  );

  Map<String, dynamic> toJson() => {
    "sort-name": sortName,
    "name": name,
    "locale": locale,
    "type": type,
    "primary": primary,
    "begin-date": beginDate,
    "end-date": endDate,
    "type-id": typeId,
  };
}

class Release {
  String? id;
  String? statusId;
  int? count;
  String? title;
  Status? status;
  ReleaseGroup? releaseGroup;
  String? date;
  String? country;
  List<ReleaseEvent>? releaseEvents;
  int? trackCount;
  List<Media>? media;
  List<ReleaseArtistCredit>? artistCredit;

  Release({
    this.id,
    this.statusId,
    this.count,
    this.title,
    this.status,
    this.releaseGroup,
    this.date,
    this.country,
    this.releaseEvents,
    this.trackCount,
    this.media,
    this.artistCredit,
  });

  factory Release.fromJson(Map<String, dynamic> json) => Release(
    id: json["id"],
    statusId: json["status-id"],
    count: json["count"],
    title: json["title"],
    status: statusValues.map[json["status"]],
    releaseGroup: json["release-group"] == null ? null : ReleaseGroup.fromJson(json["release-group"]),
    date: json["date"],
    country: json["country"],
    releaseEvents: json["release-events"] == null ? [] : List<ReleaseEvent>.from(json["release-events"]!.map((x) => ReleaseEvent.fromJson(x))),
    trackCount: json["track-count"],
    media: json["media"] == null ? [] : List<Media>.from(json["media"]!.map((x) => Media.fromJson(x))),
    artistCredit: json["artist-credit"] == null ? [] : List<ReleaseArtistCredit>.from(json["artist-credit"]!.map((x) => ReleaseArtistCredit.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "status-id": statusId,
    "count": count,
    "title": title,
    "status": statusValues.reverse[status],
    "release-group": releaseGroup?.toJson(),
    "date": date,
    "country": country,
    "release-events": releaseEvents == null ? [] : List<dynamic>.from(releaseEvents!.map((x) => x.toJson())),
    "track-count": trackCount,
    "media": media == null ? [] : List<dynamic>.from(media!.map((x) => x.toJson())),
    "artist-credit": artistCredit == null ? [] : List<dynamic>.from(artistCredit!.map((x) => x.toJson())),
  };
}

class ReleaseArtistCredit {
  String? name;
  Artist? artist;

  ReleaseArtistCredit({
    this.name,
    this.artist,
  });

  factory ReleaseArtistCredit.fromJson(Map<String, dynamic> json) => ReleaseArtistCredit(
    name: json["name"],
    artist: json["artist"] == null ? null : Artist.fromJson(json["artist"]),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "artist": artist?.toJson(),
  };
}

class Media {
  int? position;
  Format? format;
  List<Track>? track;
  int? trackCount;
  int? trackOffset;

  Media({
    this.position,
    this.format,
    this.track,
    this.trackCount,
    this.trackOffset,
  });

  factory Media.fromJson(Map<String, dynamic> json) => Media(
    position: json["position"],
    format: formatValues.map[json["format"]],
    track: json["track"] == null ? [] : List<Track>.from(json["track"]!.map((x) => Track.fromJson(x))),
    trackCount: json["track-count"],
    trackOffset: json["track-offset"],
  );

  Map<String, dynamic> toJson() => {
    "position": position,
    "format": formatValues.reverse[format],
    "track": track == null ? [] : List<dynamic>.from(track!.map((x) => x.toJson())),
    "track-count": trackCount,
    "track-offset": trackOffset,
  };
}

enum Format { CD, DIGITAL_MEDIA, THE_12_VINYL }

final formatValues = EnumValues({
  "CD": Format.CD,
  "Digital Media": Format.DIGITAL_MEDIA,
  "12\" Vinyl": Format.THE_12_VINYL
});

class Track {
  String? id;
  String? number;
  String? title;
  int? length;

  Track({
    this.id,
    this.number,
    this.title,
    this.length,
  });

  factory Track.fromJson(Map<String, dynamic> json) => Track(
    id: json["id"],
    number: json["number"],
    title: json["title"],
    length: json["length"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "number": number,
    "title": title,
    "length": length,
  };
}

class ReleaseEvent {
  String? date;
  Artist? area;

  ReleaseEvent({
    this.date,
    this.area,
  });

  factory ReleaseEvent.fromJson(Map<String, dynamic> json) => ReleaseEvent(
    date: json["date"],
    area: json["area"] == null ? null : Artist.fromJson(json["area"]),
  );

  Map<String, dynamic> toJson() => {
    "date": date,
    "area": area?.toJson(),
  };
}

class ReleaseGroup {
  String? id;
  String? typeId;
  String? primaryTypeId;
  String? title;
  PrimaryType? primaryType;
  List<dynamic>? secondaryTypes;
  List<String>? secondaryTypeIds;

  ReleaseGroup({
    this.id,
    this.typeId,
    this.primaryTypeId,
    this.title,
    this.primaryType,
    this.secondaryTypes,
    this.secondaryTypeIds,
  });

  factory ReleaseGroup.fromJson(Map<String, dynamic> json) => ReleaseGroup(
    id: json["id"],
    typeId: json["type-id"],
    primaryTypeId: json["primary-type-id"],
    title: json["title"],
    primaryType: primaryTypeValues.map[json["primary-type"]],
    secondaryTypes: json["secondary-types"] == null? [] : List<dynamic>.from(json["secondary-types"]?.map((x) => secondaryTypeValues.map[x])),
    secondaryTypeIds: json["secondary-type-ids"] == null ? [] : List<String>.from(json["secondary-type-ids"]?.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type-id": typeId,
    "primary-type-id": primaryTypeId,
    "title": title,
    "primary-type": primaryTypeValues.reverse[primaryType],
    "secondary-types": secondaryTypes == null ? [] : List<dynamic>.from(secondaryTypes!.map((x) => secondaryTypeValues.reverse[x])),
    "secondary-type-ids": secondaryTypeIds == null ? [] : List<dynamic>.from(secondaryTypeIds!.map((x) => x)),
  };
}

enum PrimaryType { ALBUM, EP }

final primaryTypeValues = EnumValues({
  "Album": PrimaryType.ALBUM,
  "EP": PrimaryType.EP
});

enum SecondaryType { COMPILATION, LIVE }

final secondaryTypeValues = EnumValues({
  "Compilation": SecondaryType.COMPILATION,
  "Live": SecondaryType.LIVE
});

enum Status { OFFICIAL, BOOTLEG, PSEUDO_RELEASE }

final statusValues = EnumValues({
  "Bootleg": Status.BOOTLEG,
  "Official": Status.OFFICIAL,
  "Pseudo-Release": Status.PSEUDO_RELEASE
});

class Tag {
  int? count;
  String? name;

  Tag({
    this.count,
    this.name,
  });

  factory Tag.fromJson(Map<String, dynamic> json) => Tag(
    count: json["count"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "name": name,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
