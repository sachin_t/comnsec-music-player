// To parse this JSON data, do
//
//     final getPopularSongsModel = getPopularSongsModelFromJson(jsonString);

import 'dart:convert';

GetPopularSongsModel getPopularSongsModelFromJson(String str) => GetPopularSongsModel.fromJson(json.decode(str));

String getPopularSongsModelToJson(GetPopularSongsModel data) => json.encode(data.toJson());

class GetPopularSongsModel {
  DateTime? created;
  int? count;
  int? offset;
  List<Recording>? recordings;

  GetPopularSongsModel({
    this.created,
    this.count,
    this.offset,
    this.recordings,
  });

  factory GetPopularSongsModel.fromJson(Map<String, dynamic> json) => GetPopularSongsModel(
    created: DateTime.parse(json["created"]),
    count: json["count"],
    offset: json["offset"],
    recordings: List<Recording>.from(json["recordings"].map((x) => Recording.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "created": created?.toIso8601String(),
    "count": count,
    "offset": offset,
    "recordings": List<dynamic>.from(recordings!.map((x) => x.toJson())),
  };
}

class Recording {
  String? id;
  int? score;
  Title? title;
  int? length;
  dynamic? video;
  List<ArtistCredit>? artistCredit;
  List<Release>? releases;
  String? firstReleaseDate;
  List<Tag>? tags;
  String? disambiguation;
  List<String>? isrcs;

  Recording({
    this.id,
    this.score,
    this.title,
    this.length,
    this.video,
    this.artistCredit,
    this.releases,
    this.firstReleaseDate,
    this.tags,
    this.disambiguation,
    this.isrcs,
  });

  factory Recording.fromJson(Map<String, dynamic> json) => Recording(
    id: json["id"],
    score: json["score"],
    title: titleValues.map[json["title"]],
    length: json["length"],
    video: json["video"],
    artistCredit: List<ArtistCredit>.from(json["artist-credit"].map((x) => ArtistCredit.fromJson(x))),
    releases: List<Release>.from(json["releases"].map((x) => Release.fromJson(x))),
    firstReleaseDate: json["first-release-date"],
    tags: List<Tag>.from(json["tags"] != null? json["tags"].map((x) => Tag.fromJson(x)):<Tag>[]),
    disambiguation: json["disambiguation"],
    isrcs: List<String>.from(json["isrcs"] != null? json["isrcs"].map((x) => x):<String>[]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "score": score,
    "title": titleValues.reverse[title],
    "length": length,
    "video": video,
    "artist-credit": List<dynamic>.from(artistCredit!.map((x) => x.toJson())),
    "releases": List<dynamic>.from(releases!.map((x) => x.toJson())),
    "first-release-date": firstReleaseDate,
    "tags": List<dynamic>.from(tags!.map((x) => x.toJson())),
    "disambiguation": disambiguation,
    "isrcs": List<dynamic>.from(isrcs!.map((x) => x)),
  };
}

class ArtistCredit {
  String? name;
  Artist? artist;
  String? joinphrase;

  ArtistCredit({
    this.name,
    this.artist,
    this.joinphrase,
  });

  factory ArtistCredit.fromJson(Map<String, dynamic> json) => ArtistCredit(
    name: json["name"],
    artist: Artist.fromJson(json["artist"]),
    joinphrase: json["joinphrase"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "artist": artist?.toJson(),
    "joinphrase": joinphrase,
  };
}

class Artist {
  String? id;
  String? name;
  String? sortName;
  String? disambiguation;
  List<Alias>? aliases;
  List<String>? iso31661Codes;

  Artist({
    this.id,
    this.name,
    this.sortName,
    this.disambiguation,
    this.aliases,
    this.iso31661Codes,
  });

  factory Artist.fromJson(Map<String, dynamic> json) => Artist(
    id: json["id"],
    name: json["name"],
    sortName: json["sort-name"],
    disambiguation: json["disambiguation"],
    aliases: List<Alias>.from(json["aliases"] != null? json["aliases"].map((x) => Alias.fromJson(x)):<Alias>[]),
    iso31661Codes: List<String>.from(json["iso-3166-1-codes"] != null?json["iso-3166-1-codes"].map((x) => x):<String>[]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "sort-name": sortName,
    "disambiguation": disambiguation,
    "aliases": List<dynamic>.from(aliases!.map((x) => x.toJson())),
    "iso-3166-1-codes": List<dynamic>.from(iso31661Codes!.map((x) => x)),
  };
}

class Alias {
  String? sortName;
  String? name;
  String? locale;
  String? type;
  bool? primary;
  DateTime? beginDate;
  dynamic? endDate;
  String? typeId;

  Alias({
    this.sortName,
    this.name,
    this.locale,
    this.type,
    this.primary,
    this.beginDate,
    this.endDate,
    this.typeId,
  });

  factory Alias.fromJson(Map<String, dynamic> json) => Alias(
    sortName: json["sort-name"],
    name: json["name"],
    locale: json["locale"],
    type: json["type"],
    primary: json["primary"],
    beginDate: DateTime.parse(json["begin-date"]??DateTime.now().toString()),
    endDate: json["end-date"],
    typeId: json["type-id"],
  );

  Map<String, dynamic> toJson() => {
    "sort-name": sortName,
    "name": name,
    "locale": locale,
    "type": type,
    "primary": primary,
    "begin-date": "${beginDate?.year.toString().padLeft(4, '0')}-${beginDate?.month.toString().padLeft(2, '0')}-${beginDate?.day.toString().padLeft(2, '0')}",
    "end-date": endDate,
    "type-id": typeId,
  };
}

class Release {
  String? id;
  String? statusId;
  int? count;
  String? title;
  Status? status;
  ReleaseGroup? releaseGroup;
  int? trackCount;
  List<Media>? media;
  List<ArtistCredit>? artistCredit;
  String? date;
  String? country;
  List<ReleaseEvent>? releaseEvents;

  Release({
    this.id,
    this.statusId,
    this.count,
    this.title,
    this.status,
    this.releaseGroup,
    this.trackCount,
    this.media,
    this.artistCredit,
    this.date,
    this.country,
    this.releaseEvents,
  });

  factory Release.fromJson(Map<String, dynamic> json) => Release(
    id: json["id"],
    statusId: json["status-id"],
    count: json["count"],
    title: json["title"],
    status: statusValues.map[json["status"]],
    releaseGroup: ReleaseGroup.fromJson(json["release-group"]),
    trackCount: json["track-count"],
    media: List<Media>.from(json["media"].map((x) => Media.fromJson(x))),
    artistCredit: List<ArtistCredit>.from(json["artist-credit"] != null?
    json["artist-credit"].map((x) => ArtistCredit.fromJson(x)):<ArtistCredit>[]),
    date: json["date"],
    country: json["country"],
    releaseEvents: List<ReleaseEvent>.from(json["release-events"] != null?
    json["release-events"].map((x) => ReleaseEvent.fromJson(x)):<ReleaseEvent>[]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "status-id": statusId,
    "count": count,
    "title": title,
    "status": statusValues.reverse[status],
    "release-group": releaseGroup?.toJson(),
    "track-count": trackCount,
    "media": List<dynamic>.from(media!.map((x) => x.toJson())),
    "artist-credit": List<dynamic>.from(artistCredit!.map((x) => x.toJson())),
    "date": date,
    "country": country,
    "release-events": List<dynamic>.from(releaseEvents!.map((x) => x.toJson())),
  };
}

class Media {
  int? position;
  List<Track>? track;
  int? trackCount;
  int? trackOffset;
  Format? format;

  Media({
    this.position,
    this.track,
    this.trackCount,
    this.trackOffset,
    this.format,
  });

  factory Media.fromJson(Map<String, dynamic> json) => Media(
    position: json["position"],
    track: List<Track>.from(json["track"].map((x) => Track.fromJson(x))),
    trackCount: json["track-count"],
    trackOffset: json["track-offset"],
    format: formatValues.map[json["format"]],
  );

  Map<String, dynamic> toJson() => {
    "position": position,
    "track": List<dynamic>.from(track!.map((x) => x.toJson())),
    "track-count": trackCount,
    "track-offset": trackOffset,
    "format": formatValues.reverse[format],
  };
}

enum Format { DIGITAL_MEDIA, CD, ENHANCED_CD, CASSETTE, THE_12_VINYL }

final formatValues = EnumValues({
  "Cassette": Format.CASSETTE,
  "CD": Format.CD,
  "Digital Media": Format.DIGITAL_MEDIA,
  "Enhanced CD": Format.ENHANCED_CD,
  "12\" Vinyl": Format.THE_12_VINYL
});

class Track {
  String? id;
  String? number;
  Title? title;
  int? length;

  Track({
    this.id,
    this.number,
    this.title,
    this.length,
  });

  factory Track.fromJson(Map<String, dynamic> json) => Track(
    id: json["id"],
    number: json["number"],
    title: titleValues.map[json["title"]],
    length: json["length"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "number": number,
    "title": titleValues.reverse[title],
    "length": length,
  };
}

enum Title { MICHAEL_JACKSON }

final titleValues = EnumValues({
  "Michael Jackson": Title.MICHAEL_JACKSON
});

class ReleaseEvent {
  String? date;
  Artist? area;

  ReleaseEvent({
    this.date,
    this.area,
  });

  factory ReleaseEvent.fromJson(Map<String, dynamic> json) => ReleaseEvent(
    date: json["date"],
    area: Artist.fromJson(json["area"]),
  );

  Map<String, dynamic> toJson() => {
    "date": date,
    "area": area?.toJson(),
  };
}

class ReleaseGroup {
  String? id;
  String? typeId;
  String? primaryTypeId;
  String? title;
  PrimaryType? primaryType;
  List<SecondaryType>? secondaryTypes;
  List<String>? secondaryTypeIds;

  ReleaseGroup({
    this.id,
    this.typeId,
    this.primaryTypeId,
    this.title,
    this.primaryType,
    this.secondaryTypes,
    this.secondaryTypeIds,
  });

  factory ReleaseGroup.fromJson(Map<String, dynamic> json) => ReleaseGroup(
    id: json["id"],
    typeId: json["type-id"],
    primaryTypeId: json["primary-type-id"],
    title: json["title"],
    primaryType: primaryTypeValues.map[json["primary-type"]],
    secondaryTypes: List<SecondaryType>.from(json["secondary-types"]!=null?
    json["secondary-types"].map((x) => secondaryTypeValues.map[x]):<SecondaryType>[]),
    secondaryTypeIds: List<String>.from(json["secondary-type-ids"]!=null?
    json["secondary-type-ids"].map((x) => x):<String>[]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type-id": typeId,
    "primary-type-id": primaryTypeId,
    "title": title,
    "primary-type": primaryTypeValues.reverse[primaryType],
    "secondary-types": List<dynamic>.from(secondaryTypes!.map((x) => secondaryTypeValues.reverse[x])),
    "secondary-type-ids": List<dynamic>.from(secondaryTypeIds!.map((x) => x)),
  };
}

enum PrimaryType { OTHER, ALBUM, EP }

final primaryTypeValues = EnumValues({
  "Album": PrimaryType.ALBUM,
  "EP": PrimaryType.EP,
  "Other": PrimaryType.OTHER
});

enum SecondaryType { SPOKENWORD, DJ_MIX, COMPILATION, LIVE }

final secondaryTypeValues = EnumValues({
  "Compilation": SecondaryType.COMPILATION,
  "DJ-mix": SecondaryType.DJ_MIX,
  "Live": SecondaryType.LIVE,
  "Spokenword": SecondaryType.SPOKENWORD
});

enum Status { OFFICIAL, PROMOTION, BOOTLEG }

final statusValues = EnumValues({
  "Bootleg": Status.BOOTLEG,
  "Official": Status.OFFICIAL,
  "Promotion": Status.PROMOTION
});

class Tag {
  int? count;
  String? name;

  Tag({
    this.count,
    this.name,
  });

  factory Tag.fromJson(Map<String, dynamic> json) => Tag(
    count: json["count"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "name": name,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
