// To parse this JSON data, do
//
//     final getArtistBySearchModel = getArtistBySearchModelFromJson(jsonString);

import 'dart:convert';

GetArtistBySearchModel getArtistBySearchModelFromJson(String str) => GetArtistBySearchModel.fromJson(json.decode(str));

String getArtistBySearchModelToJson(GetArtistBySearchModel data) => json.encode(data.toJson());

class GetArtistBySearchModel {
  DateTime? created;
  int? count;
  int? offset;
  List<Artist>? artists;

  GetArtistBySearchModel({
    this.created,
    this.count,
    this.offset,
    this.artists,
  });

  factory GetArtistBySearchModel.fromJson(Map<String, dynamic> json) => GetArtistBySearchModel(
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
    count: json["count"],
    offset: json["offset"],
    artists: json["artists"] == null ? [] : List<Artist>.from(json["artists"]!.map((x) => Artist.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "created": created?.toIso8601String(),
    "count": count,
    "offset": offset,
    "artists": artists == null ? [] : List<dynamic>.from(artists!.map((x) => x.toJson())),
  };
}

class Artist {
  String? id;
  String? type;
  String? typeId;
  int? score;
  String? genderId;
  String? name;
  String? sortName;
  String? gender;
  String? country;
  Area? area;
  Area? beginArea;
  String? disambiguation;
  List<String>? isnis;
  ArtistLifeSpan? lifeSpan;
  List<Alias>? aliases;
  List<Tag>? tags;

  Artist({
    this.id,
    this.type,
    this.typeId,
    this.score,
    this.genderId,
    this.name,
    this.sortName,
    this.gender,
    this.country,
    this.area,
    this.beginArea,
    this.disambiguation,
    this.isnis,
    this.lifeSpan,
    this.aliases,
    this.tags,
  });

  factory Artist.fromJson(Map<String, dynamic> json) => Artist(
    id: json["id"],
    type: json["type"],
    typeId: json["type-id"],
    score: json["score"],
    genderId: json["gender-id"],
    name: json["name"],
    sortName: json["sort-name"],
    gender: json["gender"],
    country: json["country"],
    area: json["area"] == null ? null : Area.fromJson(json["area"]),
    beginArea: json["begin-area"] == null ? null : Area.fromJson(json["begin-area"]),
    disambiguation: json["disambiguation"],
    isnis: json["isnis"] == null ? [] : List<String>.from(json["isnis"]!.map((x) => x)),
    lifeSpan: json["life-span"] == null ? null : ArtistLifeSpan.fromJson(json["life-span"]),
    aliases: json["aliases"] == null ? [] : List<Alias>.from(json["aliases"]!.map((x) => Alias.fromJson(x))),
    tags: json["tags"] == null ? [] : List<Tag>.from(json["tags"]!.map((x) => Tag.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type": type,
    "type-id": typeId,
    "score": score,
    "gender-id": genderId,
    "name": name,
    "sort-name": sortName,
    "gender": gender,
    "country": country,
    "area": area?.toJson(),
    "begin-area": beginArea?.toJson(),
    "disambiguation": disambiguation,
    "isnis": isnis == null ? [] : List<dynamic>.from(isnis!.map((x) => x)),
    "life-span": lifeSpan?.toJson(),
    "aliases": aliases == null ? [] : List<dynamic>.from(aliases!.map((x) => x.toJson())),
    "tags": tags == null ? [] : List<dynamic>.from(tags!.map((x) => x.toJson())),
  };
}

class Alias {
  String? sortName;
  String? name;
  dynamic locale;
  String? type;
  dynamic primary;
  dynamic beginDate;
  dynamic endDate;
  String? typeId;

  Alias({
    this.sortName,
    this.name,
    this.locale,
    this.type,
    this.primary,
    this.beginDate,
    this.endDate,
    this.typeId,
  });

  factory Alias.fromJson(Map<String, dynamic> json) => Alias(
    sortName: json["sort-name"],
    name: json["name"],
    locale: json["locale"],
    type: json["type"],
    primary: json["primary"],
    beginDate: json["begin-date"],
    endDate: json["end-date"],
    typeId: json["type-id"],
  );

  Map<String, dynamic> toJson() => {
    "sort-name": sortName,
    "name": name,
    "locale": locale,
    "type": type,
    "primary": primary,
    "begin-date": beginDate,
    "end-date": endDate,
    "type-id": typeId,
  };
}

class Area {
  String? id;
  String? type;
  String? typeId;
  String? name;
  String? sortName;
  AreaLifeSpan? lifeSpan;

  Area({
    this.id,
    this.type,
    this.typeId,
    this.name,
    this.sortName,
    this.lifeSpan,
  });

  factory Area.fromJson(Map<String, dynamic> json) => Area(
    id: json["id"],
    type: json["type"],
    typeId: json["type-id"],
    name: json["name"],
    sortName: json["sort-name"],
    lifeSpan: json["life-span"] == null ? null : AreaLifeSpan.fromJson(json["life-span"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type": type,
    "type-id": typeId,
    "name": name,
    "sort-name": sortName,
    "life-span": lifeSpan?.toJson(),
  };
}

class AreaLifeSpan {
  dynamic ended;

  AreaLifeSpan({
    this.ended,
  });

  factory AreaLifeSpan.fromJson(Map<String, dynamic> json) => AreaLifeSpan(
    ended: json["ended"],
  );

  Map<String, dynamic> toJson() => {
    "ended": ended,
  };
}

class ArtistLifeSpan {
  String? begin;
  dynamic ended;

  ArtistLifeSpan({
    this.begin,
    this.ended,
  });

  factory ArtistLifeSpan.fromJson(Map<String, dynamic> json) => ArtistLifeSpan(
    begin: json["begin"],
    ended: json["ended"],
  );

  Map<String, dynamic> toJson() => {
    "begin": begin,
    "ended": ended,
  };
}

class Tag {
  int? count;
  String? name;

  Tag({
    this.count,
    this.name,
  });

  factory Tag.fromJson(Map<String, dynamic> json) => Tag(
    count: json["count"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "name": name,
  };
}
