// To parse this JSON data, do
//
//     final getAllGenreModel = getAllGenreModelFromJson(jsonString);

import 'dart:convert';

GetAllGenreModel getAllGenreModelFromJson(String str) => GetAllGenreModel.fromJson(json.decode(str));

String getAllGenreModelToJson(GetAllGenreModel data) => json.encode(data.toJson());

class GetAllGenreModel {
  List<Genre>? genres;
  int? genreCount;
  int? genreOffset;

  GetAllGenreModel({
    this.genres,
    this.genreCount,
    this.genreOffset,
  });

  factory GetAllGenreModel.fromJson(Map<String, dynamic> json) => GetAllGenreModel(
    genres: List<Genre>.from(json["genres"].map((x) => Genre.fromJson(x))),
    genreCount: json["genre-count"],
    genreOffset: json["genre-offset"],
  );

  Map<String, dynamic> toJson() => {
    "genres": List<dynamic>.from(genres!.map((x) => x.toJson())),
    "genre-count": genreCount,
    "genre-offset": genreOffset,
  };
}

class Genre {
  String? id;
  String? disambiguation;
  String? name;

  Genre({
    this.id,
    this.disambiguation,
    this.name,
  });

  factory Genre.fromJson(Map<String, dynamic> json) => Genre(
    id: json["id"],
    disambiguation: json["disambiguation"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "disambiguation": disambiguation,
    "name": name,
  };
}
