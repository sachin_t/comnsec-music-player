import 'dart:ui';

import 'package:get/get.dart';

double height = Get.height;
double width = Get.width;

const Color appBlack = Color(0xff000000);
const Color appTextWhite = Color(0xffffffff);
const Color appLightPurple = Color(0xdd660279);
