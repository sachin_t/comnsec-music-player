
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:musically/model/get_Artist_by_search_model.dart';
import 'package:musically/model/get_song_by_artist_model.dart'as Recordings;

import '../main.dart';
import '../service/api_service.dart';
import '../view/search_result_page.dart';

class SearchPageController extends GetxController {

  RxList<String> recentSearchList = <String>[].obs;
  RxList<Artist> searchResultList = <Artist>[].obs;
  RxList<Recordings.Recording> songList = <Recordings.Recording>[].obs;
  RxBool isLoading = false.obs;
  String artistName = '';
  TextEditingController searchController = TextEditingController();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    recentSearchList.value = prefs.getStringList('recentSearchList')??[];
  }

  void getArtistBySearchQuery({required String searchText}) async {
    isLoading.value = true;
    searchResultList.value.clear();
    if (searchText.isNotEmpty) {
      var data = await ApiService.getArtistBySearchQuery(searchText: searchText);
      searchResultList.value = data.artists??<Artist>[];
    }

    isLoading.value = false;
    update();
  }

  void getSongByArtist({required String artistName}) async {
    isLoading.value = true;
    artistName = artistName;
    var data = await ApiService.getSongByArtist(artistName: artistName);
    songList.value = data.recordings??<Recordings.Recording>[];
    recentSearchList.insert(0, artistName);
    prefs.setStringList('recentSearchList', recentSearchList??[]);
    update();
    searchResultList.clear();
    isLoading.value = false;
    Get.to(SearchResultPage(artistName: artistName));
    update();
  }

}
