import 'package:get/get.dart';
import 'package:musically/service/api_service.dart';

import '../model/get_all_genre_model.dart';

class HomeController extends GetxController {

  RxBool isLoading = false.obs;

  List<Genre> musicGenreList = [];
  List<String> recordingsList = [];
  List<String> artistNameList = [];

  @override
  void onInit() {
    super.onInit();

    getAllGenre();

  }

  void getAllGenre() async {
    isLoading.value = true;

    var data = await ApiService.getAllGenre();

    if (data.genres != []) {
      musicGenreList = data.genres??[];
    }

    getPopularSongs();

    update();
  }

  void getPopularSongs() async {
    var data = await ApiService.getPopularSongs();

    for (var element in data.recordings!) {

      recordingsList.add(element.releases?[0].title??'');

      artistNameList.add(element.artistCredit?[0].artist?.name??'');
    }

    isLoading.value = false;

    update();
  }
}